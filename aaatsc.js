/**
 * Created by pligor on 5/28/15.
 */
Array.prototype.getClosestTo = function (num) {
    return this.reduce(function (prev, curr) {
        return (Math.abs(curr - num) < Math.abs(prev - num) ? curr : prev);
    });
};
Array.prototype.distinct = function () {
    /*return this.reduce(function(p, c) {
     if (p.indexOf(c) < 0) p.push(c);
     return p;
     }, []);*/
    return this.filter(function (elem, index, arr) {
        return arr.indexOf(elem) == index;
    });
};
Array.prototype.unique = Array.prototype.distinct;
Array.prototype.sum = function () {
    return this.reduce(function (n1, n2) {
        return n1 + n2;
    }, 0);
};
Array.prototype.flat = function () {
    var flatarray = [];
    function func(arg) {
        if (Array.isArray(arg)) {
            for (var i = 0; i < arg.length; i++) {
                func(arg[i]);
            }
        }
        else {
            flatarray.push(arg);
        }
    }
    func(this);
    return flatarray;
};
/**
 * Created by gpligoropoulos on 10/10/2015.
 */
function log(str) {
    console.log(str);
}
/**
 * Created by gpligoropoulos on 10/10/2015.
 */
String.prototype.repeat = function (num) {
    return new Array(num + 1).join(this);
};
/**
 * Created by gpligoropoulos on 10/10/2015.
 */
///<reference path="./../myts/array.ts"/>
///<reference path="./../myts/generic.ts"/>
///<reference path="./../myts/string.ts"/>
var n = 61;
var Staircase;
(function (Staircase) {
    var Symbol;
    (function (Symbol) {
        Symbol[Symbol["EmptySpace"] = 0] = "EmptySpace";
        Symbol[Symbol["Hash"] = 1] = "Hash";
    })(Symbol || (Symbol = {}));
    var SymbolContainer = (function () {
        function SymbolContainer(s) {
            if (s === Symbol.EmptySpace || s === Symbol.Hash) {
                this.symbol = s;
            }
            else if (s instanceof String) {
                var symbol = s.substring(0, 1);
                this.symbol = symbol === SymbolContainer.hashString ? Symbol.Hash : Symbol.EmptySpace;
            }
            else {
                throw new Error("tried to initialize Symbol Container without being correct");
            }
        }
        SymbolContainer.prototype.toString = function () {
            return this.symbol === Symbol.EmptySpace ? SymbolContainer.emptyString : SymbolContainer.hashString;
        };
        SymbolContainer.emptyString = " ";
        SymbolContainer.hashString = "#";
        return SymbolContainer;
    }());
    var RepeatableSymbol = (function () {
        function RepeatableSymbol(n, symbol) {
            this.count = 0;
            this.count = n;
            var c = Symbol.EmptySpace;
            this.symbols = symbol.toString().repeat(this.count);
        }
        RepeatableSymbol.prototype.toString = function () {
            return this.symbols;
        };
        return RepeatableSymbol;
    }());
    function create(n) {
        var str = "";
        for (var i = 1; i <= n; i++) {
            str += new RepeatableSymbol(n - i, new SymbolContainer(Symbol.EmptySpace)).toString() +
                new RepeatableSymbol(i, new SymbolContainer(Symbol.Hash)).toString() + "\n";
        }
        return str;
    }
    Staircase.create = create;
})(Staircase || (Staircase = {}));
log(Staircase.create(n));
//# sourceMappingURL=aaatsc.js.map
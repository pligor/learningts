/**
 * Created by pligor on 6/4/15.
 */

/// <reference path="declarations/jquery.d.ts" />
/// <reference path="declarations/underscore.d.ts" />
/// <reference path="myts/array" />
/// <reference path="myts/object" />

function assert(check) {
    if(check === true) {
        //nop
    } else {
        throw "assertion failed"
    }
}

function log(str) {
    console.log(str)
}

$(() => {
    $("body").css("background-color", "#454545")


    log([3, 2, 11].sum())
})
/**
 * Created by pligor on 6/3/15.
 */

/// <reference path="./ElectricityCalc.ts" />

var calc = new CO2calc()

console.log(
    calc.getKgCO2rounded(10000)
)

//////////////////////////////////////////////


var calc = new ElectricityCalculationsG1();

var energyYear = 10000
calc.electricityCostYearly(energyYear, true)

var costYear = calc.electricityCostYearly(energyYear, true)

console.log("cost 25 years: " + Math.round(costYear * 20.7))



/////////////////////////////////////////////////////////////////


var calc = new Oikiaka(10000)

console.log(
    "kWp: " + calc.getkWp()
)

console.log(
    "price: " + calc.getPrice()
)

console.log(
    "aposvesi: " + calc.formatYears(
        calc.getAposvesiYears()
    )
)

console.log(
    "profitable: " + calc.formatYears(
        calc.getProfitableYears()
    )
)


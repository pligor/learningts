/**
 * Created by gpligoropoulos on 10/9/2015.
 */

///<reference path="./../myts/generic.ts"/>

var n = 3

var str =
`11 2 4
4 5 6
10 8 -12`

//console.log(str.split("\n"))

let a = [];

let sumTLtoBR = 0
let sumTRtoBL = 0

for(let a_i = 0; a_i < n; a_i++){
    a[a_i] = str.split("\n")[a_i].split(' ');
    a[a_i] = a[a_i].map(Number);

    let line = a[a_i]

    sumTLtoBR += line[a_i]

    sumTRtoBL += line[n - 1 - a_i]
}

let result = Math.abs(sumTLtoBR - sumTRtoBL)

log(result)

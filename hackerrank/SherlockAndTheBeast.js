/**
 * Created by student on 27/5/2016.
 */

//number of 3s divisible by 5
//number of 5s divisible by 3

function generateNumber(countFive, countThree) {
    var fiveString = ""

    for (var i = 0; i < countFive; i++) {
        fiveString = fiveString + "5"
    }

    var threeString = ""

    for (var j = 0; j < countThree; j++) {
        threeString = threeString + "3"
    }

    var finalString = fiveString + threeString

    if(finalString == "") {
        return -1
    } else {
        return parseInt(finalString)
    }
}

function decent(N) {
    var maxFives = N - (N % 3)

    var maxThrees = N - (N % 5)

    //take all the combinations of divisible by 5 and divisible by 3

    var fives = []

    var threes = []

    for (var i = 0; i <= maxFives; i += 3) {
        fives.push(i)
    }

    for (var j = 0; j <= maxThrees; j += 5) {
        threes.push(j)
    }

    var maxNumber = -1;

    for (var k in fives) {
        for (var m in threes) {
            var curFiveCount = fives[k]

            var curThreeCount = threes[m]

            if(curFiveCount + curThreeCount <= N) {
                var curNum = generateNumber(fives[k], threes[m])

                maxNumber = Math.max(curNum, maxNumber)
            } else {
                //skip
            }
        }
    }

    return maxNumber;
}

function decentMore(N) {
    var maxFives = N - (N % 3)

    var maxThrees = N - (N % 5)

    //take all the combinations of divisible by 5 and divisible by 3

    var maxNumber = -1;

    for (var i = 0; i <= maxFives; i += 3) {
        var curFiveCount = i

        for (var j = 0; j <= maxThrees; j += 5) {
            var curThreeCount = j

            if(curFiveCount + curThreeCount <= N) {
                var curNumOrNot = parseInt("5".repeat(curFiveCount) + "3".repeat(curThreeCount)) //generateNumber(curFiveCount, curThreeCount)

                var curNum = isNaN(curNumOrNot) ? -1 : curNumOrNot

                maxNumber = Math.max(curNum, maxNumber)
            } else {
                //skip
            }
        }
    }

    return maxNumber;
}

String.prototype.repeat = function( num )
{
    return new Array( num + 1 ).join( this );
}

function isValid(countFives, countThrees) {
    return (countFives % 3 === 0) && (countThrees % 5 === 0)
}

function reallyDecent(N) {
    if(N >= 3) {
        var fives = N

        var threes = 0

        while(!isValid(fives, threes)) {
            fives -= 5

            threes += 5

            if(threes > N) {
                break;
            }
        }

        if(isValid(fives, threes)) {
            return "5".repeat(fives) + "3".repeat(threes)
        } else {
            return -1
        }
    } else {
        return -1
    }
}


console.log(
    reallyDecent(1) //decent(1) == decentMore(1)
)

console.log(
    reallyDecent(3) //decent(3) == decentMore(3)
)

console.log(
    reallyDecent(5) //decent(5) == decentMore(5)
)

console.log(
    reallyDecent(11) //decent(11) == decentMore(11)
)


/*5
2194
12002
21965
55140
57634*/

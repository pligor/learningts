/**
 * Created by pligor on 27/5/2016.
 */

function getMinimumUniqueSumSimple(arr) {

    //count square integers in the interval

    function isInt(value) {
        return !isNaN(value) && parseInt(Number(value)) == value && !isNaN(parseInt(value, 10));
    }

    //console.log(isInt(10))
    //console.log(isInt(10.1))


    for (var i in arr) {
        var testcase = arr[i].split(" ")

        var count = 0;

        for (var j = testcase[0]; j <= testcase[1]; j++) {
            if (isInt(Math.sqrt(j))) {
                count += 1
            }
        }

        console.log(count)
    }

}


function getMinimumUniqueSum(arr) {

    //count square integers in the interval

    function isInt(value) {
        return !isNaN(value) && parseInt(Number(value)) == value && !isNaN(parseInt(value, 10));
    }


    for (var i in arr) {
        var testcase = arr[i].split(" ")

        var count = 0;

        var min = testcase[0]

        var max = testcase[1]

        function findFirst() {
            for (var j = min; j <= max; j++) {
                var sqrt = Math.sqrt(j)

                if (isInt(sqrt)) {
                    count += 1

                    return sqrt
                }
            }

            return null
        }

        var cur = findFirst()

        if (cur) {
            cur += 1

            //console.log("cur: " + cur)

            while (Math.pow(cur, 2) <= max) {
                cur += 1

                count += 1
            }
        } else {
            //nop
        }

        console.log(count)
    }

}

//getMinimumUniqueSum(['3 9', '17 24'])


function doesCircleExist(commands) {
    //G go forward
    //L turn left
    //R turn right

    //check if at the end is where it started
    //coordinates

    var cmdArr = commands.split('')

    var rotations = ["R", "U", "L", "D"]

    var curRot = "R"

    var pointing = {
        x: 1,
        y: 0
    }

    var coords = {
        x: 0,
        y: 0
    }

    for (var i in cmdArr) {
        var char = cmdArr[i]

        switch (char) {
            case "G":

                switch (curRot) {
                    case "R":
                        coords.x = coords.x + 1

                        break;
                    case "U":
                        coords.y = coords.y + 1

                        break;
                    case "L":
                        coords.x = coords.x - 1

                        break;
                    case "D":
                        coords.y = coords.y - 1

                        break;
                }

                break;
            case "L":
                curRot = rotations[(rotations.indexOf(curRot) + 1) % rotations.length]

                //console.log("curRot: " + curRot)

                break;
            case "R":
                var temp = (rotations.indexOf(curRot) - 1)

                temp = (temp < 0) ? rotations.length - 1 : temp

                curRot = rotations[temp]

                //console.log("curRot: " + curRot)

                break;
        }
    }

    if (coords.x == 0 && coords.y == 0) {
        return "YES"
    } else {
        return "NO"
    }
}

/*
 java:
 private static int isPresent(Node root, int val){
 if(root == null) {
 return 0;
 } else {
 if(root.data == val) {
 return 1;
 } else {
 int lefts = isPresent(root.left, val);
 int rights = isPresent(root.right, val);

 if(lefts == 1 || rights == 1) {
 return 1;
 } else {
 return 0;
 }
 }
 }
 }
 */


/**
 * Created by gpligoropoulos on 10/10/2015.
 */

///<reference path="./../myts/array.ts"/>

///<reference path="./../myts/generic.ts"/>

///<reference path="./../myts/string.ts"/>

var n = 61

module Staircase {
    enum Symbol {EmptySpace = 0, Hash = 1}

    class SymbolContainer {
        private symbol:Symbol

        private static emptyString = " "

        private static hashString = "#"

        constructor(s) {
            if (s === Symbol.EmptySpace || s === Symbol.Hash) {
                this.symbol = s
            } else if (s instanceof String) {
                let symbol = s.substring(0, 1)

                this.symbol = symbol === SymbolContainer.hashString ? Symbol.Hash : Symbol.EmptySpace
            } else {
                throw new Error("tried to initialize Symbol Container without being correct")
            }
        }

        toString():string {
            return this.symbol === Symbol.EmptySpace ? SymbolContainer.emptyString : SymbolContainer.hashString
        }
    }

    class RepeatableSymbol {
        private symbols:string

        private count = 0

        constructor(n:number, symbol:SymbolContainer) {
            this.count = n

            var c:Symbol = Symbol.EmptySpace

            this.symbols = symbol.toString().repeat(this.count)
        }

        toString():string {
            return this.symbols
        }
    }

    export function create(n:number):string {
        var str = ``

        for (let i = 1; i <= n; i++) {
            str += new RepeatableSymbol(n - i, new SymbolContainer(Symbol.EmptySpace)).toString() +
                new RepeatableSymbol(i, new SymbolContainer(Symbol.Hash)).toString() + "\n"
        }

        return str
    }
}

log(
    Staircase.create(n)
)
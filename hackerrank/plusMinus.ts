/**
 * Created by gpligoropoulos on 10/10/2015.
 */

///<reference path="./../myts/generic.ts"/>

var n = 6

var arr = [-4, 3, -9, 0, 4, 1]

class PlusMinus {
    private howManyIntegers:number

    private integersArray:number[]

    private positivesCount = 0
    private zerosCount = 0
    private negativesCount = 0

    constructor(n:number, arr:number[]) {
        this.howManyIntegers = n;

        this.integersArray = arr
    }

    renderFractions(fractions: [number, number, number]) {
        //fractions
        fractions.forEach(function(num: number, index, arr) {
            log(num.toFixed(6))
        })
    }

    calcFractions(): [number,number, number] {
        for (let i = 0; i < this.howManyIntegers; i++) {
            let curInt = this.integersArray[i]

            this.addIfNegative(curInt)
            this.addIfZero(curInt)
            this.addIfPositive(curInt)
        }

        return [
            this.positivesCount / this.howManyIntegers,
            this.negativesCount / this.howManyIntegers,
            this.zerosCount / this.howManyIntegers,
        ]
    }

    private addIfZero(num:number): void {
        if (num === 0) {
            this.zerosCount += 1
        } else {
            //nop
        }
    }

    private addIfPositive(num:number): void {
        if (num > 0) {
            this.positivesCount += 1
        } else {
            //nop
        }
    }

    private addIfNegative(num:number): void {
        if (num < 0) {
            this.negativesCount += 1
        } else {
            //nop
        }
    }
}

var obj = new PlusMinus(n, arr)

var fractions = obj.calcFractions()

obj.renderFractions(fractions)


/**
 * Created by student on 27/5/2016.
 */

var heightBuffer = {
    0: 1,   //cycle -> height
}

var saveHeights = {}

/*function sortSavedHeights() {
    saveHeights.sort(function (a, b) {
        return a.cycles - b.cycles
    })
}*/

function cycleOdd(height) {
    return 2 * height
}

function cycleEven(height) {
    return 1 + height
}

function findInHeights(cycles) {
    //sortSavedHeights()

    //var curBuffer = null

    var curCycles = cycles;

    while( !(curCycles in saveHeights) ) {
        curCycles -= 1;

        if(curCycles < 0) {
            break;
        }
    }

    if(curCycles < 0) {
        return null
    } else {
        return {
            cycles: curCycles,
            height: saveHeights[curCycles],
        }
    }

    /*for (var i = 0; i < saveHeights.length; i++) {
        var savedHeight = saveHeights[i]

        if (savedHeight.cycles <= cycles) {
            curBuffer = {
                cycles: savedHeight.cycles,
                height: savedHeight.height
            }
        } else {
            //break;
        }
    }*/

    //return curBuffer
}

function solution(n) {
     var buffered = findInHeights(n)

    var startCycle = buffered ? buffered.cycles : 1

    var curHeight = buffered ? buffered.height : 1

    for (var c = startCycle; c <= n; c++) {
        saveHeights[c] = curHeight

        var curFunc = (c % 2 === 0) ? cycleEven : cycleOdd;

        curHeight = curFunc(curHeight)
    }

    return curHeight
}

console.log(
    solution(0)
)

console.log(
    solution(1)
)

console.log(
    solution(4)
)
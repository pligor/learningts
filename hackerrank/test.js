/**
 * Created by gpligoropoulos on 10/10/2015.
 */

var Color;
(function (Color) {
    Color[Color["Red"] = 0] = "Red";
    Color[Color["Green"] = 10] = "Green";
    Color[Color["Blue"] = 11] = "Blue";
})(Color || (Color = {}));


console.log(Color.Red)
console.log(Color.Green)
console.log(Color.Blue)

/**
 * Created by student on 27/5/2016.
 */

//find 2D patterns

//T test cases
T = 2;

function solution(G, R, C, P, r, c) {
    var curSkip = 0;

    while (true) {
        //console.log("curSkip: " + curSkip)

        var tuple = solutionBySkipping(G, R, C, P, r, c, curSkip)

        var result = tuple[0]

        var somethingFound = tuple[1]

        if (result) {
            return "YES"
        } else {
            if (somethingFound == false) {
                console.log("somethingFound == false")
                return "NO"
            } else {
                //console.log(curSkip)
                curSkip += 1
            }
        }
    }
}

function solutionBySkipping(G, R, C, P, r, c, skipHowMany) {
    var somethingFound = false

    var curP = 0

    var curIndex = -1

    function reset() {
        curIndex = -1

        curP = 0
    }

    function nextOrFound() {
        curP += 1;

        return curP == r
    }

    function findNthOccurrence(i) {
        var skip = skipHowMany || 0

        var ind;

        var curString = G[i]

        do {
            ind = curString.indexOf(P[curP])

            if (ind >= 0) {
                curString = curString.substring(ind + 1)
            } else {
                break;
            }

            skip -= 1

        } while (skip >= 0)

        if (ind >= 0) {
            somethingFound = true
        } else {
            //nop
        }

        return ind
    }

    var result = false

    var index = 0

    var gindex = null

    while (index < R) {
        //console.log(index)

        if (curIndex >= 0) {
            var nextInd = G[index].indexOf(P[curP])

            if (nextInd == curIndex) {
                if (nextOrFound()) {
                    result = true

                    break
                }
            } else {
                index = gindex

                reset()
            }
        } else {
            curIndex = findNthOccurrence(index)

            if (curIndex >= 0) {
                gindex = index

                if (nextOrFound()) {
                    result = true

                    break
                }
            } else {
                reset()
            }
        }

        index += 1
    }

    return [result, somethingFound]
}

(function () {
    var R = 4;   //rows
    var C = 6;    //columns

    var r = 2;
    var c = 2;

    var G = ['123412',
        '561212',
        '123634',
        '781288'];

    var P = ['12',
        '34'];

    console.log(
        solution(G, R, C, P, r, c) + "-YES"
    )
})();   //YES

 (function () {
 var R = 1;   //rows
 var C = 10;    //columns

 var r = 1;
 var c = 4;

 var G = [/*'7283455864',
 '6731158619',
 '8988242643',
 '3830589324',*/
 '2229505813',
 //'5633845374',
 //'6473530293',
 /*'7053106601',
 '0834282956',
 '4607924137'*/];

 var P = ['9505', /*'3845', '3530'*/];

 console.log(
 solution(G, R, C, P, r, c)+"-YES"
 )
 })();   //YES

 (function () {
 var R = 1;   //rows
 var C = 10;    //columns

 var r = 3;
 var c = 4;

 var G = [/*'7283455864',
 '6731158619',
 '8988242643',
 '3830589324',*/
 '2229505813',
 //'5633845374',
 //'6473530293',
 /*'7053106601',
 '0834282956',
 '4607924137'*/];

 var P = ['9505', '3845', '3530'];

 console.log(
 solution(G, R, C, P, r, c)+"-NO"
 )
 })();

 (function () {
 var R = 2;   //rows
 var C = 10;    //columns

 var r = 3;
 var c = 4;

 var G = [/*'7283455864',
 '6731158619',
 '8988242643',
 '3830589324',*/
 '2229505813',
 '5633845374',
 //'6473530293',
 /*'7053106601',
 '0834282956',
 '4607924137'*/];

 var P = ['9505', '3845', '3530'];

 console.log(
 solution(G, R, C, P, r, c)+"-NO"
 )
 })();

 (function () {
 var R = 3;   //rows
 var C = 10;    //columns

 var r = 3;
 var c = 4;

 var G = [/*'7283455864',
 '6731158619',
 '8988242643',
 '3830589324',*/
 '2229505813',
 '5633845374',
 '6473530293',
 /*'7053106601',
 '0834282956',
 '4607924137'*/];

 var P = ['9505', '3845', '3530'];

 console.log(
 solution(G, R, C, P, r, c)+"-YES"
 )
 })();

 (function () {
 var R = 10;   //rows
 var C = 10;    //columns

 var r = 1;
 var c = 4;

 var G = ['7283455864',
 '6731158619',
 '8988242643',
 '3830589324',
 '2229505813',
 '5633845374',
 '6473530293',
 '7053106601',
 '0834282956',
 '4607924137'];

 var P = ['9505', /*'3845', '3530'*/];

 console.log(
 solution(G, R, C, P, r, c)+"-YES"
 )
 })();

 (function () {
 var R = 10;   //rows
 var C = 10;    //columns

 var r = 3;
 var c = 4;

 var G = ['7283455864',
 '6731158619',
 '8988242643',
 '3830589324',
 '2229505813',
 '5633845374',
 '6473530293',
 '7053106601',
 '0834282956',
 '4607924137'];

 var P = ['9505', '3845', '3530'];

 console.log(
 solution(G, R, C, P, r, c)+"-YES"
 )
 })();

 (function () {
 var R = 15;   //rows
 var C = 15;    //columns

 var r = 2;
 var c = 2;

 var G = ['400453592126560',
 '114213133098692',
 '474386082879648',
 '522356951189169',
 '887109450487496',
 '252802633388782',
 '502771484966748',
 '075975207693780',
 '511799789562806',
 '404007454272504',
 '549043809916080',
 '962410809534811',
 '445893523733475',
 '768705303214174',
 '650629270887160']

 var P = ['99', '99']

 console.log(
 solution(G, R, C, P, r, c) +"-NO"
 )
 })();

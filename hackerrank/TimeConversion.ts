/**
 * Created by gpligoropoulos on 10/10/2015.
 */

///<reference path="./../myts/array.ts"/>
///<reference path="./../myts/generic.ts"/>
///<reference path="./../myts/string.ts"/>

let time = "12:00:00PM"

//00:00:00 -> 12 am

//12:00:00 -> 12 pm

module TimeConversion {
    let pmOrAmIndex = 8

    let hourDigits = 2

    let hours = parseInt(time.substr(0, hourDigits))

    let innerTime = time.substring(hourDigits, pmOrAmIndex)

    let pmOrAm = time.substr(pmOrAmIndex)


    let hours24 = (hours + (pmOrAm === "PM" ? 12 : 0)) % 24

    let hours24fixed = (hours === 12 && pmOrAm === "AM") ? 0 : ((hours === 12 && pmOrAm === "PM") ? 12 : hours24)

    let hours24formatted:string = (hours24fixed < 10 ? "0" : "") + hours24fixed

    let result = hours24formatted + innerTime

    /*log(pmOrAm)
     log(innerTime)
     log(hours)
     log(hours24)*/

    log(result)
}

/**
 * Created by student on 27/5/2016.
 */

var queue = function (funcs, scope = this) {
    (function next() {
        if (funcs.length > 0) {
            var f = funcs.shift();
            f.apply(scope, [next].concat(Array.prototype.slice.call(arguments, 0)));
        }
    })();
};

var obj = {
    value: null
};

var funcs:any = [
    function (callback) {
        var self = this;
        setTimeout(function () {
            self.value = 10;
            callback(20);
        }, 200)
    },
    function (callback, add) {
        console.log(this.value + add);
        callback();
    },
    function () {    //an example of the scope
        console.log(obj.value);
    }
]

//queue(funcs, obj)

function binding(x:number) {    //original
    if (x > 0) {
        console.log("ok")
    } else {
        console.log("not ok")
    }
}

function argConverter(args:Array<any>):number {
    return args.pop()
}

////////////////////////

function funcModified(callback, x:number) {    //modified
    if (x > 0) {
        setTimeout(function () {
            console.log(x * 10)
            callback()
        }, 1200)
    } else {
        console.log(x * -10)
        callback()
    }
}

function funcWrapper(func, ...args) {
    return function (callback) {
        func(callback, argConverter(args))
    }
}

var bindFuncs:any = []

function wrapper(x:number) {    //replacement
    bindFuncs.push(funcWrapper(funcModified, x))
}

wrapper(3)
wrapper(-5)
wrapper(7)
wrapper(-10)

//console.log(
queue(bindFuncs)
//)



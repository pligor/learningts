/**
 * Created by pligor on 5/28/15.
 */

interface Object {
    getValues(): any[];
}

Object.prototype.getValues = function(): any[] {
    var keys = Object.getOwnPropertyNames(this)
    var values = []
    for (var i = 0; i < keys.length; i++) {
        values.push(this[keys[i]])
    }
    return values
}
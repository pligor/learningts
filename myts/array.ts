/**
 * Created by pligor on 5/28/15.
 */

interface Array<T> {
    getClosestTo(num: number): number;

    distinct(): any[]

    unique(): any[]

    sum(): number

    flat(): any[]
}

Array.prototype.getClosestTo = function(num: number) {
    return this.reduce((prev: number, curr: number) => {
        return (Math.abs(curr - num) < Math.abs(prev - num) ? curr : prev);
    });
}

Array.prototype.distinct = function<T>(): any[] {
    /*return this.reduce(function(p, c) {
     if (p.indexOf(c) < 0) p.push(c);
     return p;
     }, []);*/
    return this.filter((elem: any, index: number, arr: Array<T>) => {
        return arr.indexOf(elem) == index
    })
}

Array.prototype.unique = Array.prototype.distinct

Array.prototype.sum = function(): number {
    return this.reduce((n1: number, n2: number) => {
        return n1 + n2
    }, 0)
}

Array.prototype.flat = function(): any[] {
    let flatarray: any[] = []

    function func(arg: any) {
        if(Array.isArray(arg)) {
            for(var i=0; i< arg.length; i++) {
                func(arg[i])
            }
        } else {
            flatarray.push(arg)
        }
    }

    func(this)

    return flatarray
}


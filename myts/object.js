Object.prototype.getValues = function () {
    var keys = Object.getOwnPropertyNames(this);
    var values = [];
    for (var i = 0; i < keys.length; i++) {
        values.push(this[keys[i]]);
    }
    return values;
};

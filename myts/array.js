Array.prototype.getClosestTo = function (num) {
    return this.reduce(function (prev, curr) {
        return (Math.abs(curr - num) < Math.abs(prev - num) ? curr : prev);
    });
};
Array.prototype.distinct = function () {
    return this.filter(function (elem, index, arr) {
        return arr.indexOf(elem) == index;
    });
};
Array.prototype.unique = Array.prototype.distinct;
Array.prototype.sum = function () {
    return this.reduce(function (n1, n2) {
        return n1 + n2;
    }, 0);
};
Array.prototype.flat = function () {
    var flatarray = [];
    function func(arg) {
        if (Array.isArray(arg)) {
            for (var i = 0; i < arg.length; i++) {
                func(arg[i]);
            }
        }
        else {
            flatarray.push(arg);
        }
    }
    func(this);
    return flatarray;
};

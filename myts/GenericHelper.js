function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}
var GenericHelper = (function () {
    function GenericHelper() {
    }
    GenericHelper.getObjectValues = function (obj) {
        var keys = Object.getOwnPropertyNames(obj);
        var values = [];
        for (var i = 0; i < keys.length; i++) {
            values.push(obj[keys[i]]);
        }
        return values;
    };
    GenericHelper.areObjectsEqual = function (a, b) {
        var curLen = Object.keys(a).length;
        if (curLen == Object.keys(b).length) {
            var aKeys = Object.getOwnPropertyNames(a);
            var bKeys = Object.getOwnPropertyNames(b);
            var keyCheck = true;
            for (var i = 0; i < curLen; i++) {
                keyCheck = keyCheck && (aKeys[i] == bKeys[i]);
            }
            if (keyCheck) {
                var aValues = GenericHelper.getObjectValues(a);
                var bValues = GenericHelper.getObjectValues(b);
                var valueCheck = true;
                for (var j = 0; j < curLen; j++) {
                    var aValue = aValues[j];
                    var bValue = bValues[j];
                    var curType = typeof (aValue);
                    var typeOfBvalue = typeof (bValue);
                    if (curType == typeOfBvalue) {
                        if (curType == "object") {
                            valueCheck = valueCheck && GenericHelper.areObjectsEqual(aValue, bValue);
                        }
                        else if (curType == "function") {
                        }
                        else {
                            valueCheck = valueCheck && (aValue == bValue);
                        }
                    }
                    else {
                        valueCheck = false;
                    }
                }
                return valueCheck;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    };
    GenericHelper.test_areObjectsEqual = function () {
        var hello = {
            number1: 66,
            mymethod: function () {
                return 33;
            },
            number2: {
                x: 0,
                y: 0
            }
        };
        var world = {
            number1: 66,
            mymethod: function () {
                return true;
            },
            number2: {
                x: 0,
                y: 0
            }
        };
    };
    GenericHelper.getRange = function (start, step, stop, inclusive) {
        if (inclusive === void 0) { inclusive = true; }
        var steps = [];
        for (var i = inclusive ? start : start + step; inclusive ? i <= stop : i < stop; i += step) {
            steps.push(i);
        }
        return steps;
    };
    return GenericHelper;
}());

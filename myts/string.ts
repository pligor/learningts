/**
 * Created by gpligoropoulos on 10/10/2015.
 */

interface String {
    repeat(num:number): string
}

String.prototype.repeat = function (num:number) {
    return new Array(num + 1).join(this);
}
/**
 * Created by pligor on 5/2/15.
 */
//DO NOT FORGET TO `npm install shelljs` in the same folder (locally) before executing

//YOU ALSO NEED TO DOWNLOAD THIS FROM HERE: https://github.com/borisyankov/DefinitelyTyped/tree/master/shelljs
///<reference path="shelljs.d.ts"/>

//var shelljs = require('shelljs')//require('shelljs/global')   //this is only if you do not use the declaration file
import shelljs = require('shelljs')

//http://www.sitepoint.com/accessing-the-file-system-in-node-js/
var fs = require("fs");

var TARGET_COLOR = "#AABBCC"

var TARGET_FILE_EXT = ".png";

var filenames:Array<String> = fs.readdirSync(".")

var imgFilenames = filenames.filter((filename, index, array) => {
    return filename.slice(filename.length - TARGET_FILE_EXT.length) === TARGET_FILE_EXT
})

var getBaseName = (filename:String, fileExtension) => {
    return filename.slice(0, filename.length - fileExtension.length)
}

var cmd = (inputFilename:String, targetColor:string) => {
    var curExtension = inputFilename.slice(inputFilename.lastIndexOf("."))

    return "convert \\( " + inputFilename + " -alpha extract \\) -background \"" + targetColor + "\" -alpha shape " +
        getBaseName(inputFilename, curExtension) + "_" + targetColor + curExtension
    //remember the spaces are important in this command
}

console.log(imgFilenames)

imgFilenames.map((imgFilename, index, array) => {
    return cmd(imgFilename, TARGET_COLOR)
}).forEach((commandString, index, array) => {
    console.log(commandString)
    shelljs.exec(commandString, {silent: true})
})

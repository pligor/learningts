"use strict";
var shelljs = require('shelljs');
var fs = require("fs");
var TARGET_COLOR = "#AABBCC";
var TARGET_FILE_EXT = ".png";
var filenames = fs.readdirSync(".");
var imgFilenames = filenames.filter(function (filename, index, array) {
    return filename.slice(filename.length - TARGET_FILE_EXT.length) === TARGET_FILE_EXT;
});
var getBaseName = function (filename, fileExtension) {
    return filename.slice(0, filename.length - fileExtension.length);
};
var cmd = function (inputFilename, targetColor) {
    var curExtension = inputFilename.slice(inputFilename.lastIndexOf("."));
    return "convert \\( " + inputFilename + " -alpha extract \\) -background \"" + targetColor + "\" -alpha shape " +
        getBaseName(inputFilename, curExtension) + "_" + targetColor + curExtension;
};
console.log(imgFilenames);
imgFilenames.map(function (imgFilename, index, array) {
    return cmd(imgFilename, TARGET_COLOR);
}).forEach(function (commandString, index, array) {
    console.log(commandString);
    shelljs.exec(commandString, { silent: true });
});

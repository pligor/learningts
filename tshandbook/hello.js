function greeter(person) {
    return "Hello, " + person.firstname + " " + person.lastname;
}
var user = {
    firstname: "Tasos",
    lastname: "Koumoutsakos"
};
console.log(greeter(user));
var Student = (function () {
    function Student(firstname, middleinitial, lastname) {
        this.firstname = firstname;
        this.middleinitial = middleinitial;
        this.lastname = lastname;
        this.fullname = firstname + " " + middleinitial + " " + lastname;
    }
    return Student;
}());
var myuser = new Student("George", 2, "Secret");
console.log(myuser.fullname);

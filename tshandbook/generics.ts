/**
 * Created by pligor on 4/16/15.
 */

function log(s:any):void {
    console.log(s)
}

function echo<T>(arg:T):T {
    return arg
}

log(echo("echoooooooooooooooo"))
log(echo<string>("echoooooooooooooooo"))    //too elaborate but ok
//log(echo<number>("echoooooooooooooooo"))    //fails to compile .. obviously

function loggingIdentity<T>(arg:T[]):Array<T> {   //T[] and Array<T> are equivalent
    log(arg.length)
    return arg
}

//THREE equivalent declarations vvvvvvvvvvvvv
//1)
var myEcho:<T>(arg:T) => T = echo;
//2)
var yourEcho:{<T>(arg:T): T} = echo;
//3)
interface GenericIdentityFn {
    <T>(arg:T): T;
}
var myIdentity:GenericIdentityFn = echo;
//THREE equivalent declarations ^^^^^^^^^^^^^


class GenericNumber<T> {
    zeroValue:T;
    add:(x:T, y:T) => T;
}

var myGenericNumber = new GenericNumber<string>();
myGenericNumber.zeroValue = "";
myGenericNumber.add = function (x, y) {
    return x + y
}

log(
    myGenericNumber.add("nai etsi ", myGenericNumber.add(myGenericNumber.zeroValue, "ola kala"))
)

interface LengthWise {
    length: number
}

function genericLogger<T extends LengthWise>(arg:T):T {
    log(arg.length)
    return arg
}

log(
    genericLogger([39, 5, 1, 22])
    //in scala this would not work because array does not implement LengthWise but here things are not that strict
)

log(
    genericLogger({param: 20, length: 2})   //this works because length is present ;)
)


//this is how you can get generics with the class as parameter
function create<T>(cl:{new(): T;}):T {
    return new cl();
}
//TODO I do not understand how to use the above function!
//var newInstance = create(GenericNumber).add(1, 3)
//log(newInstance)


class BeeKeeper {
    hasMask:boolean;
}
class ZooKeeper {
    nametag:string;
    constructor(newNameTag: string) {
        this.nametag = newNameTag
    }
}

class Animal {
    numLengs:number;
}
class Bee extends Animal {
    keeper:BeeKeeper;
}
class Lion extends Animal {
    keeper:ZooKeeper;
    /*constructor(keeperName: string) {
        super();
        this.keeper = new ZooKeeper(keeperName);
    }*/
}

//We define that the type is A and that its prototype has a variable named keeper
function findKeeper<A extends Animal, K>(a:{new(): A;prototype: {keeper: K}}):K {
    return a.prototype.keeper
}

var keeper = findKeeper(Lion).nametag   //TODO typechecks but it does not provide useful info. WHY?
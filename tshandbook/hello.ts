/**
 * Created by pligor on 4/15/15.
 */

interface Person {
    firstname: string;
    lastname: string;
}

function greeter(person: Person) {
    return "Hello, " + person.firstname + " " + person.lastname;
}

//var user = [2, 5, 6];

//var user = new Person("Tasos", "Koumoutsakos");   //fails
var user = {
    firstname: "Tasos",
    lastname: "Koumoutsakos"
};

//document.body.innerHTML = greeter(user);
console.log(
    greeter(user)
)

class Student {
    fullname: string;

    constructor(public firstname, public middleinitial, public lastname) {
        this.fullname = firstname + " " + middleinitial + " " + lastname;
    }
}

var myuser = new Student("George", 2, "Secret")

console.log(
    myuser.fullname
)



/**
 * Created by pligor on 4/15/15.
 */

///<reference path="./../myts/generic.ts"/>
var list1:number[] = [1, 2, 3];

//console.log(list.concat == [1, 2, 3] ? "lista complete" : "lista gtp")

var list2:Array<number> = [1, 2, 3]

enum Color {Red, Green = 10, Blue}  //implies 0, 10, 11

var c: Color = Color.Red

log(Color.Green)

var getcolorByIndex = Color[10]

log(getcolorByIndex)

var notSure: any = 4;
notSure = "maybe a string instead"
notSure = false;

log(notSure)

var list:any[] = [1, true, "free"]
list[1] = 100;

var warn = () => log("warning!!")

warn()

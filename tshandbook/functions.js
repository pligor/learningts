function log(s) {
    console.log(s);
}
function add(x, y) {
    return x + y;
}
var myAdd = function (x, y) {
    return x + y;
};
log(myAdd(3, 4) == add(4, 3));
var yourAdd = function (x, y) {
    return x + y;
};
function buildName(firstname, lastname) {
    if (lastname == undefined) {
        return firstname + " noname";
    }
    else {
        return firstname + " " + lastname;
    }
}
log(buildName("George"));
log(buildName("George", "pl"));
function nameBuilder(firstname, lastname) {
    if (lastname === void 0) { lastname = "NoName"; }
    return firstname + " " + lastname;
}
log(nameBuilder("George"));
log(nameBuilder("George", "pl"));
function longNameBuilder(first) {
    var restOfName = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        restOfName[_i - 1] = arguments[_i];
    }
    return first + " " + restOfName.join(" ");
}
log(longNameBuilder("Geo", "Leo", "Alabama", "Acapoulko"));
log(longNameBuilder("Geo"));
var deck = {
    suits: ["hearts", "spades", "clubs", "diamonds"],
    cards: Array(52),
    createCardPicker: function () {
        var _this = this;
        return function () {
            var pickedCard = Math.floor(Math.random() * 52);
            var pickedSuit = Math.floor(pickedCard / 13);
            return { suit: _this.suits[pickedSuit], card: pickedCard % 13 };
        };
    }
};
var cardPicker = deck.createCardPicker();
var pickedCard = cardPicker();
log("card: " + pickedCard.card + " of " + pickedCard.suit);
var suits = ["hearts", "spades", "clubs", "diamonds"];
function pickCard(x) {
    if (typeof x == "object") {
        return Math.floor(Math.random() * x.length);
    }
    else if (typeof x == "number") {
        return { suit: suits[Math.floor(x / 13)], card: x % 13 };
    }
    else {
        return undefined;
    }
}
log(pickCard(33));
var myDeck = [{ suit: "hearts", card: 3 }, { suit: "diamonds", card: 10 }];
log(pickCard(myDeck));

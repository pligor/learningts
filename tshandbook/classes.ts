/**
 * Created by pligor on 4/16/15.
 */

function log(str):void {
    console.log(str)
}

//how could I have inner classes? see in declaration merging for that

class Animal {
    private name:string;

    constructor(theName:string) {
        this.name = theName;
    }

    move(meters:number = 0):void {
        log(this.name + " has moved " + meters + "m")
    }
}

class Snake extends Animal {
    constructor(name:string) {
        super(name)
    }

    move(meters = 5) {
        log("slithering")
        super.move(meters)
    }
}

class Horse extends Animal {
    constructor(name:string) {
        super(name);
    }

    move(meters = 45) {
        log("Galloping")
        super.move(meters)
    }
}

var sam = new Snake("Sammy the Python")
var tom:Animal = new Horse("Tommy the Palomino")

//log(sam.name) //because we have it private
tom.move(54)

class Employee {
    private name:string;

    constructor(theName:string) {
        this.name = theName
    }
}

var animal = new Animal("Rhino King")
var employee = new Employee("employee of the month")

//animal = employee;  //in order for this not to fail all the members of the class must be public and must be the same!
//here Employee has private the name as Animal does, and also Employee is missing the "move" method!


//if you are having problem with getters and setters: http://stackoverflow.com/a/27675870/720484
var passcode = "secretcode"

class Car {
    private _brandName:string = "noname";

    get brandName():string {
        return this._brandName
    }

    set brandName(newName:string) {
        if (passcode && passcode == "secret code") {
            this._brandName = newName
        } else {
            log("unauthorized")
        }
    }
}

var car = new Car();

car.brandName = "Ford"

if (car.brandName) {
    log(car.brandName)
}

class Grid {
    static origin = {x: 0, y: 0}

    calculateDistanceFromOrigin(point:{x: number; y:number}):number {
        var xDist = point.x - Grid.origin.x;
        var yDist = point.y - Grid.origin.y;

        return Math.sqrt(xDist * xDist + Math.pow(yDist, 2)) / this.scale
    }

    constructor(public scale:number) {

    }
}

var grid1 = new Grid(1.0)   //1x scale
var grid2 = new Grid(2.0)   //2x scale

log(grid1.calculateDistanceFromOrigin({x: 10, y: 10}))
log(grid2.calculateDistanceFromOrigin({x: 4, y: 3}))

class Greeter {
    static standardGreeting = "Hello, there"

    greeting:string;

    constructor(message:string) {
        this.greeting = message
    }

    greet() {
        return "Hello, " + this.greeting
    }
}

var greeter:Greeter;
greeter = new Greeter("people")
log(greeter.greet())

var greeterMaker:typeof Greeter = Greeter;
greeterMaker.standardGreeting = "Hey there!"
var greeter2:Greeter = new greeterMaker("nobody")
log(greeterMaker.standardGreeting)  //we have changed that ;)
log(greeter2.greet())       //this works ok

class Point {
    x:number
    y:number
}

class Point3d extends Point {
    z:number
}

var point3d_explicitly = new Point3d()
point3d_explicitly.x = 33
point3d_explicitly.y = -4
point3d_explicitly.z = 0.1

var point3d_implicitly:Point3d = {x: 33, y: -4, z: 0.1}

log(point3d_explicitly === point3d_implicitly)


log(Object.getOwnPropertyNames(point3d_explicitly))

/**
 * Created by pligor on 4/16/15.
 */

function log(s:any):void {
    console.log(s)
}

//mixin
class Disposable {
    isDisposed: boolean;
    dispose() {
        this.isDisposed = true;
    }
}

//mixin
class Activatable {
    isActive: boolean;
    activate() {
        this.isActive = true;
    }
    deactivate() {
        this.isActive = false;
    }
}

class SmartObject implements Disposable, Activatable {
    //implement disposable
    isDisposed: boolean = false;
    dispose: () => void;    //dispose is inherited already

    isActive: boolean = false;
    activate: () => void
    deactivate: () => void

    constructor() {
        this.disp();
    }

    interact() {
        this.activate()
    }

    disp(): void {
        log(this.isActive + ": " + this.isDisposed)
    }
}
applyMixins(SmartObject, [Disposable, Activatable])

var smartObj = new SmartObject()

smartObj.interact()

smartObj.disp()

//THIS FUNCTION SHOULD BE APPLIED ALWAYS WHEN MIXINS ARE INVOLVED
function applyMixins(derivedCtor: any, baseCtors: any[]) {
    baseCtors.forEach(baseCtor => {
        Object.getOwnPropertyNames(baseCtor.prototype).forEach(name => {
            derivedCtor.prototype[name] = baseCtor.prototype[name];
        })
    })
}
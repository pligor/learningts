/**
 * Created by pligor on 4/15/15.
 */

function log(str):void {
    console.log(str)
}

interface LabelledValue {
    label: string;
}

function printLabel(labelObj:LabelledValue) {
    log(labelObj.label)
}

var myObj = {size: 10, label: "Size 10 Object"}
printLabel(myObj)

interface SquareConfig {
    color?: string;
    width?: number;
}

function createSquare(config:SquareConfig):{color: string; area: number} {
    var newSquare = {color: "white", area: 100};

    if (config.color) {
        newSquare.color = config.color;
    }

    if (config.width) {
        newSquare.area = config.width * config.width;
    }

    return newSquare;
}

interface SearchFunc {
    (source:string, subString:string): boolean;
}

var mySearch:SearchFunc;

//note the name of the params do not have to match subString != mySubString
mySearch = function (source:string, mySubString:string) {
    var result = source.search(mySubString);

    return result != -1;
}

interface StringArray {
    [index: number]: string;
}

var myArray:StringArray;
myArray = ["Bob", "Fred"];

interface Dictionary {
    [index: string]: string;    //and you can also have an arbitrary other
    length: string; //this must be provided, but it can only be a subtype (or same type), like a number or a string
    //this also works if number and string are interchanged
}
// this is a little confusing!
var mydic:Dictionary = {
    length: "2222",
    34355: "yes ok"
}

log(mydic)

interface ClockInterface {
    currentTime: Date;
    setTime(d:Date);
}

class Clock implements ClockInterface {
    currentTime:Date;

    setTime(d:Date) {
        this.currentTime = d;
    }

    constructor(protected h:number, protected m:number) {

    }

    public getTime():string {
        return this.h + ":" + this.m
    }
}

interface ClockStatic {
    new (hour:number, minute:number);
    getTime(): string;
}

//TODO this should not have been an error (??). Report it
//var myclock: ClockStatic = Clock;   //here we get a smaller version of the class, with only what is defined ?
//var newClock = new myclock(4, 6)
//log(newClock.getTime())

interface Shape {
    color: string;
}

interface Square extends Shape {
    sideLength: number;
}

var square = <Square>{};
square.color = "blue";
square.sideLength = 10;

interface PenStroke {
    penWidth: number;
}

interface Triangle extends Shape, PenStroke {   //multiple inheritance
    sideLength: number;
}

var triangle = <Triangle>{};
triangle.color = "blue";
triangle.penWidth = 5.0;
triangle.sideLength = 10;

//When interacting with 3rd-party JavaScript, you may need to use patterns like the above to
// fully-describe the shape of the type.
interface Counter {
    (start:number): string;    //the function part (function acts like a class here)
    interval: number;           //the property of the function
    reset(): void;              //the method of the function
}
//http://stackoverflow.com/questions/25340601/typescript-hybrid-type-implementation
var c:Counter = <Counter>function (start:number):string {
    this.interval = start; //from specs

    this.reset = function () {
        this.interval = 0;
    }

    this.heeee = function () {   //out of specs

    }

    return "current interval is " + this.interval;
}

//var mycounter = new c(10)
var mycounter = c(10)   //practically because the function is not void we cannot call the "new" keyword
//therefore the rest of the interface gets lost

log(mycounter)


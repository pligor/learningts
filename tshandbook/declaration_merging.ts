/**
 * Created by pligor on 4/16/15.
 */

function log(s:any):void {
    console.log(s)
}

//starting from here
interface Box {
    height: number;
    width: number;
}

interface Box {
    scale: number;
    //width: number;    //though this causes compile error because of same name
}
//the above is merging the two interfaces. No compile error! wow!
//we get this ->
/*interface Box {
    height: number;
    width: number;
    scale: number;
}*/



//for functions is ok to be same name. it is treated as overload

//starting from here
interface Document { //<--------- this function comes third
    createElement(tagName: any): Element;
}
interface Document { //<--------- this function comes second
    createElement(tagName: string): HTMLElement;
}
interface Document { //<--------- these functions come first
    createElement(tagName: "div"): HTMLDivElement;
    createElement(tagName: "span"): HTMLSpanElement;
    createElement(tagName: "canvas"): HTMLCanvasElement;
}
//we get this --->
/*interface Document { //<--------- these functions come first
    createElement(tagName: "div"): HTMLDivElement;
    createElement(tagName: "span"): HTMLSpanElement;
    createElement(tagName: "canvas"): HTMLCanvasElement;

    createElement(tagName: string): HTMLElement;

    createElement(tagName: any): Element;
}*/


//merging two modules
//BUT  Non-exported members are only visible in the original (un-merged) module
module Animals {
    var haveMuscles = true; //non-exported variable

    export class Zebra {}

    export function animalsHaveMuscles() {
        return haveMuscles;
    }
}

module Animals {
    export interface Legged {numberOfLegs: number;}
    export class Dog {}

    export function doAnimalsHaveMuscles() {    //name of function CANNOT be the same
        //return haveMuscles; //compiler error, not export, therefore not visible in export
        return animalsHaveMuscles() //ok because is exported (merged from above
    }
}
//results to this -->
/*module Animals {
    //interfaces
    export interface Legged {numberOfLegs: number;}

    //and then classes
    export class Zebra {}
    export class Dog {}
}*/


//how to have inner classes: Merging a module with a class
class Album {
    label: Album.AlbumLabel;
}
module Album {
    export class AlbumLabel {}  //visibility is only possible if exported
}
/*class Album {
    label: Album.AlbumLabel;
    /!*inner class, even though this syntax is not allowed in typescript*!/ class AlbumLabel {}
}*/

//JavaScript practice of creating a function and then extending the function further by adding properties onto the function.
// TypeScript uses declaration merging to build up definitions like this in a type-safe way.
function buildLabel(name: string): string {
    return buildLabel.prefix + name + buildLabel.suffix
}
module buildLabel {
    export var suffix = "!";
    export var prefix = "Hello, "
}

log(buildLabel("Pina Colada"))


//Extend enums
enum Color {
    red = 1,
    green = 2,
    blue = 4
}

module Color {
    export function mixRedGreen(): number {
        return Color.red + Color.green
    }
}

log(Color.mixRedGreen())
function log(str) {
    console.log(str);
}
function printLabel(labelObj) {
    log(labelObj.label);
}
var myObj = { size: 10, label: "Size 10 Object" };
printLabel(myObj);
function createSquare(config) {
    var newSquare = { color: "white", area: 100 };
    if (config.color) {
        newSquare.color = config.color;
    }
    if (config.width) {
        newSquare.area = config.width * config.width;
    }
    return newSquare;
}
var mySearch;
mySearch = function (source, mySubString) {
    var result = source.search(mySubString);
    return result != -1;
};
var myArray;
myArray = ["Bob", "Fred"];
var mydic = {
    length: "2222",
    34355: "yes ok"
};
log(mydic);
var Clock = (function () {
    function Clock(h, m) {
        this.h = h;
        this.m = m;
    }
    Clock.prototype.setTime = function (d) {
        this.currentTime = d;
    };
    Clock.prototype.getTime = function () {
        return this.h + ":" + this.m;
    };
    return Clock;
}());
var square = {};
square.color = "blue";
square.sideLength = 10;
var triangle = {};
triangle.color = "blue";
triangle.penWidth = 5.0;
triangle.sideLength = 10;
var c = function (start) {
    this.interval = start;
    this.reset = function () {
        this.interval = 0;
    };
    this.heeee = function () {
    };
    return "current interval is " + this.interval;
};
var mycounter = c(10);
log(mycounter);

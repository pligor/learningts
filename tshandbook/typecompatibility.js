function log(s) {
    console.log(s);
}
var var1 = { name: "Geo" };
var var2 = { name: "Alice", location: "Seattle" };
var1 = var2;
log(var1.name);
function greet(n) {
    log("Hello " + n);
}
greet(var2);
var func1 = function (a) { return 0; };
var func2 = function (b, s) { return 0; };
func2 = func1;
var items = [1, 2, 3];
items.forEach(function (item, index, array) { return log(index + ": " + item); });
items.forEach(function (item) { return log(item); });
var func3 = function () { return ({ name: "Alice" }); };
var func4 = function () { return ({ name: "Alice", location: "Dublin" }); };
func3 = func4;
var EventType;
(function (EventType) {
    EventType[EventType["Mouse"] = 0] = "Mouse";
    EventType[EventType["Keyboard"] = 1] = "Keyboard";
})(EventType || (EventType = {}));
function listenEvent(eventType, handler) {
}
listenEvent(EventType.Mouse, function (e) { return console.log(e.x + ',' + e.y); });
listenEvent(EventType.Mouse, function (e) { return log(e.x + "," + e.y); });
listenEvent(EventType.Mouse, (function (e) { return log(e.x + "," + e.y); }));

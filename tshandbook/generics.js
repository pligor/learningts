var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
function log(s) {
    console.log(s);
}
function echo(arg) {
    return arg;
}
log(echo("echoooooooooooooooo"));
log(echo("echoooooooooooooooo"));
function loggingIdentity(arg) {
    log(arg.length);
    return arg;
}
var myEcho = echo;
var yourEcho = echo;
var myIdentity = echo;
var GenericNumber = (function () {
    function GenericNumber() {
    }
    return GenericNumber;
}());
var myGenericNumber = new GenericNumber();
myGenericNumber.zeroValue = "";
myGenericNumber.add = function (x, y) {
    return x + y;
};
log(myGenericNumber.add("nai etsi ", myGenericNumber.add(myGenericNumber.zeroValue, "ola kala")));
function genericLogger(arg) {
    log(arg.length);
    return arg;
}
log(genericLogger([39, 5, 1, 22]));
log(genericLogger({ param: 20, length: 2 }));
function create(cl) {
    return new cl();
}
var BeeKeeper = (function () {
    function BeeKeeper() {
    }
    return BeeKeeper;
}());
var ZooKeeper = (function () {
    function ZooKeeper(newNameTag) {
        this.nametag = newNameTag;
    }
    return ZooKeeper;
}());
var Animal = (function () {
    function Animal() {
    }
    return Animal;
}());
var Bee = (function (_super) {
    __extends(Bee, _super);
    function Bee() {
        _super.apply(this, arguments);
    }
    return Bee;
}(Animal));
var Lion = (function (_super) {
    __extends(Lion, _super);
    function Lion() {
        _super.apply(this, arguments);
    }
    return Lion;
}(Animal));
function findKeeper(a) {
    return a.prototype.keeper;
}
var keeper = findKeeper(Lion).nametag;

/**
 * Created by pligor on 4/16/15.
 */

function log(s) {
    console.log(s)
}

function add(x, y) {
    return x + y
}

var myAdd = function (x, y) {
    return x + y
}

log(myAdd(3, 4) == add(4, 3))


var yourAdd:(x:number, y:number) => number = function (x:number, y:number):number {
    return x + y;
}

function buildName(firstname:string, lastname?:string) {
    if (lastname == undefined) {
        return firstname + " noname"
    } else {
        return firstname + " " + lastname
    }
}

log(buildName("George"))
log(buildName("George", "pl"))

function nameBuilder(firstname:string, lastname:string = "NoName") {
    return firstname + " " + lastname
}

log(nameBuilder("George"))
log(nameBuilder("George", "pl"))

function longNameBuilder(first:string, ...restOfName:string[]) {
    return first + " " + restOfName.join(" ")
}

log(
    longNameBuilder("Geo", "Leo", "Alabama", "Acapoulko")
)

log(
    longNameBuilder("Geo")
)


var deck = {
    suits: ["hearts", "spades", "clubs", "diamonds"],
    cards: Array(52),
    createCardPicker: function () {
        //javascript dummy solution, or we need the bind solution which is still ugly
        //var myobj = this;
        //return function() {

        return () => {
            var pickedCard = Math.floor(Math.random() * 52);
            var pickedSuit = Math.floor(pickedCard / 13);

            return {suit: this.suits[pickedSuit], card: pickedCard % 13};
        }
    }
}

var cardPicker = deck.createCardPicker();
var pickedCard = cardPicker();

log("card: " + pickedCard.card + " of " + pickedCard.suit);

var suits = ["hearts", "spades", "clubs", "diamonds"];

function pickCard(x: {suit: string; card: number;}[]): number;
function pickCard(x: number): {suit: string; card: number;};
function pickCard(x): any {
    if(typeof x == "object") {
        return Math.floor(Math.random() * x.length);
    } else if(typeof x == "number") {
        return {suit: suits[Math.floor(x/13)], card: x % 13 };
    } else {
        return undefined    //throw exception later
    }
}

log(pickCard(33))

var myDeck = [{suit: "hearts", card: 3}, {suit: "diamonds", card: 10}]
log(pickCard(myDeck))

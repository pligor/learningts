/**
 * Created by pligor on 4/16/15.
 */

function log(str):void {
    console.log(str)
}

module Validation {
    //We specify which objects are visible outside the module by using the export keyword on a top-level declaration
    export interface StringValidator {
        isAcceptable(s:string): boolean
    }

    var lettersRegexp = /^[A-Za-z]+$/
    var numberRegexp = /^[0-9]+$/

    export class LettersOnlyValidator implements StringValidator {
        isAcceptable(s:string) {
            return lettersRegexp.test(s)
        }
    }

    export class ZipCodeValidator implements StringValidator {

        isAcceptable(s:string):boolean {
            return numberRegexp.test(s) && (s.length === 5)
        }
    }

}

var strings = ["Hello", "98052", "101"]

var validators:{
    [s: string]: Validation.StringValidator
} = {};

validators["ZIP code"] = new Validation.ZipCodeValidator()
validators["Letters only"] = new Validation.LettersOnlyValidator()

strings.forEach(s => {
    //log(s)
    for (var name in validators) {
        log(s + " " + (validators[name].isAcceptable(s) ? "matches" : "does not match") + " with validator " + name)
    }
})

//this does not define any implementation (it only has interfaces) <---- this is called Ambient Internal Module
declare module D3 {
    export interface Selectors {
        select: {
            (selector: string): Selection
            (element: EventTarget): Selection
        };
    }

    export interface Event {
        x: number
        y: number
    }

    export interface Base extends Selectors {
        event: Event
    }
}

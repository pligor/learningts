var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
function log(str) {
    console.log(str);
}
var Animal = (function () {
    function Animal(theName) {
        this.name = theName;
    }
    Animal.prototype.move = function (meters) {
        if (meters === void 0) { meters = 0; }
        log(this.name + " has moved " + meters + "m");
    };
    return Animal;
}());
var Snake = (function (_super) {
    __extends(Snake, _super);
    function Snake(name) {
        _super.call(this, name);
    }
    Snake.prototype.move = function (meters) {
        if (meters === void 0) { meters = 5; }
        log("slithering");
        _super.prototype.move.call(this, meters);
    };
    return Snake;
}(Animal));
var Horse = (function (_super) {
    __extends(Horse, _super);
    function Horse(name) {
        _super.call(this, name);
    }
    Horse.prototype.move = function (meters) {
        if (meters === void 0) { meters = 45; }
        log("Galloping");
        _super.prototype.move.call(this, meters);
    };
    return Horse;
}(Animal));
var sam = new Snake("Sammy the Python");
var tom = new Horse("Tommy the Palomino");
tom.move(54);
var Employee = (function () {
    function Employee(theName) {
        this.name = theName;
    }
    return Employee;
}());
var animal = new Animal("Rhino King");
var employee = new Employee("employee of the month");
var passcode = "secretcode";
var Car = (function () {
    function Car() {
        this._brandName = "noname";
    }
    Object.defineProperty(Car.prototype, "brandName", {
        get: function () {
            return this._brandName;
        },
        set: function (newName) {
            if (passcode && passcode == "secret code") {
                this._brandName = newName;
            }
            else {
                log("unauthorized");
            }
        },
        enumerable: true,
        configurable: true
    });
    return Car;
}());
var car = new Car();
car.brandName = "Ford";
if (car.brandName) {
    log(car.brandName);
}
var Grid = (function () {
    function Grid(scale) {
        this.scale = scale;
    }
    Grid.prototype.calculateDistanceFromOrigin = function (point) {
        var xDist = point.x - Grid.origin.x;
        var yDist = point.y - Grid.origin.y;
        return Math.sqrt(xDist * xDist + Math.pow(yDist, 2)) / this.scale;
    };
    Grid.origin = { x: 0, y: 0 };
    return Grid;
}());
var grid1 = new Grid(1.0);
var grid2 = new Grid(2.0);
log(grid1.calculateDistanceFromOrigin({ x: 10, y: 10 }));
log(grid2.calculateDistanceFromOrigin({ x: 4, y: 3 }));
var Greeter = (function () {
    function Greeter(message) {
        this.greeting = message;
    }
    Greeter.prototype.greet = function () {
        return "Hello, " + this.greeting;
    };
    Greeter.standardGreeting = "Hello, there";
    return Greeter;
}());
var greeter;
greeter = new Greeter("people");
log(greeter.greet());
var greeterMaker = Greeter;
greeterMaker.standardGreeting = "Hey there!";
var greeter2 = new greeterMaker("nobody");
log(greeterMaker.standardGreeting);
log(greeter2.greet());
var Point = (function () {
    function Point() {
    }
    return Point;
}());
var Point3d = (function (_super) {
    __extends(Point3d, _super);
    function Point3d() {
        _super.apply(this, arguments);
    }
    return Point3d;
}(Point));
var point3d_explicitly = new Point3d();
point3d_explicitly.x = 33;
point3d_explicitly.y = -4;
point3d_explicitly.z = 0.1;
var point3d_implicitly = { x: 33, y: -4, z: 0.1 };
log(point3d_explicitly === point3d_implicitly);
log(Object.getOwnPropertyNames(point3d_explicitly));

/**
 * Created by pligor on 4/16/15.
 */
//these do not define any implementation (it only has interfaces) <---- they're called Ambient External Modules

declare module "url" {  //<---- this is a library of the node.js, we have it the same name as our interface
    export interface Url {
        protocol?: string;
        hostname?: string;
        pathname?: string;
    }

    export function parse(urlStr: string, parseQueryString?, slashesDenoteHost?): Url;
}

declare module "path" {
    export function normalize(p: string): string;
    export function join(...paths: any[]): string;
    export var sep: string;
}
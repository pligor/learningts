"use strict";
var url = require("url");
var zip = require("./ZipCodeValidator");
var ZipValid = zip.ZipCodeValidator;
function log(str) {
    console.log(str);
}
var strings = ["Hello", "98052", "101"];
var validators = {};
validators["ZIP code"] = new ZipValid();
var needLetterValidation = true;
if (needLetterValidation) {
    var myLetterValidator = require("./LettersOnlyValidator");
    validators["Letters only"] = new myLetterValidator();
}
else {
}
strings.forEach(function (s) {
    for (var name in validators) {
        log(s + " " + (validators[name].isAcceptable(s) ? "matches" : "does not match") + " with validator " + name);
    }
});
var myUrl = url.parse("http://www.in.gr");
log(myUrl);

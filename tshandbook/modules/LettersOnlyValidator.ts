/**
 * Created by pligor on 4/16/15.
 */
import validation = require("/home/pligor/projects/avocarrot/pligoropoulos_project/learning_ts/tshandbook/modules/Validation")

//TODO if you remove the prefix "validation." there is no error in the IDE
/*export class LettersOnlyValidator implements validation.StringValidator {
    private static lettersRegexp = /^[A-Za-z]+$/

    isAcceptable(s:string) {
        return LettersOnlyValidator.lettersRegexp.test(s)
    }
}*/

//export the entire class ^^^^^^^^^
// or
// export the class explicitly to simplify things vvvvvvvvvvvvvv

class LettersOnlyValidator implements validation.StringValidator {
    private static lettersRegexp = /^[A-Za-z]+$/

    isAcceptable(s:string) {
        return LettersOnlyValidator.lettersRegexp.test(s)
    }
}

export = LettersOnlyValidator
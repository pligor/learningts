/**
 * Created by pligor on 4/16/15.
 */
import validation = require("/home/pligor/projects/avocarrot/pligoropoulos_project/learning_ts/tshandbook/modules/Validation")

//REPORT THIS: if you remove the prefix "validation." there is no error in the IDE
export class ZipCodeValidator implements validation.StringValidator {
    private static numberRegexp = /^[0-9]+$/

    isAcceptable(s:string):boolean {
        return ZipCodeValidator.numberRegexp.test(s) && (s.length === 5)
    }
}
/**
 * Created by pligor on 4/16/15.
 *
 * THE REFERENCE PATH SHOULD BE AT THE TOP
 */
///<reference path="./OtherAmbientModules.ts"/>
import url = require("url") //<---- this is a library of the node.js, we have it the same name as our interface

import validation = require("./Validation")
import zip = require("./ZipCodeValidator")
//import letters = require("./LettersOnlyValidator")

//alias to simplify for zip:
import ZipValid = zip.ZipCodeValidator

function log(str):void {
    console.log(str)
}

var strings = ["Hello", "98052", "101"]

var validators:{
    [s: string]: validation.StringValidator
} = {};

//validators["ZIP code"] = new zip.ZipCodeValidator()
validators["ZIP code"] = new ZipValid()
//both of the above are valid

var needLetterValidation = true

//DYNAMIC IMPORT HAPPENS WITH ONE TWO THREE STATEMENTS:
declare var require;    //ONE
import Lett = require("./LettersOnlyValidator");    //TWO
if(needLetterValidation) {
    // = require("./LettersOnlyValidator")
    var myLetterValidator: typeof Lett = require("./LettersOnlyValidator")  //THREE

    //validators["Letters only"] = new letters.LettersOnlyValidator()
    validators["Letters only"] = new myLetterValidator()
    //only the uncommented is currently valid
} else {
    //nop
}

strings.forEach(s => {
    //log(s)
    for (var name in validators) {
        log(s + " " + (validators[name].isAcceptable(s) ? "matches" : "does not match") + " with validator " + name)
    }
})

/*function Url() {
    this.parse = function(urlStr, parseQueryString, slashesDenoteHost) {
        return urlStr
    }
}*/

//So we work with the existing library
var myUrl = url.parse("http://www.in.gr")

log(myUrl)
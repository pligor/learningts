"use strict";
var ZipCodeValidator = (function () {
    function ZipCodeValidator() {
    }
    ZipCodeValidator.prototype.isAcceptable = function (s) {
        return ZipCodeValidator.numberRegexp.test(s) && (s.length === 5);
    };
    ZipCodeValidator.numberRegexp = /^[0-9]+$/;
    return ZipCodeValidator;
}());
exports.ZipCodeValidator = ZipCodeValidator;

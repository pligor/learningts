"use strict";
var LettersOnlyValidator = (function () {
    function LettersOnlyValidator() {
    }
    LettersOnlyValidator.prototype.isAcceptable = function (s) {
        return LettersOnlyValidator.lettersRegexp.test(s);
    };
    LettersOnlyValidator.lettersRegexp = /^[A-Za-z]+$/;
    return LettersOnlyValidator;
}());
module.exports = LettersOnlyValidator;

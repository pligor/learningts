/**
 * Created by pligor on 4/16/15.
 */
//this does not define any implementation (it only has interfaces) <---- this is called Ambient Module
export interface StringValidator {
    isAcceptable(s:string): boolean
}
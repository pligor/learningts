function log(s) {
    console.log(s);
}
var Disposable = (function () {
    function Disposable() {
    }
    Disposable.prototype.dispose = function () {
        this.isDisposed = true;
    };
    return Disposable;
}());
var Activatable = (function () {
    function Activatable() {
    }
    Activatable.prototype.activate = function () {
        this.isActive = true;
    };
    Activatable.prototype.deactivate = function () {
        this.isActive = false;
    };
    return Activatable;
}());
var SmartObject = (function () {
    function SmartObject() {
        this.isDisposed = false;
        this.isActive = false;
        this.disp();
    }
    SmartObject.prototype.interact = function () {
        this.activate();
    };
    SmartObject.prototype.disp = function () {
        log(this.isActive + ": " + this.isDisposed);
    };
    return SmartObject;
}());
applyMixins(SmartObject, [Disposable, Activatable]);
var smartObj = new SmartObject();
smartObj.interact();
smartObj.disp();
function applyMixins(derivedCtor, baseCtors) {
    baseCtors.forEach(function (baseCtor) {
        Object.getOwnPropertyNames(baseCtor.prototype).forEach(function (name) {
            derivedCtor.prototype[name] = baseCtor.prototype[name];
        });
    });
}

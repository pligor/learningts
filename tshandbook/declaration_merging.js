function log(s) {
    console.log(s);
}
var Animals;
(function (Animals) {
    var haveMuscles = true;
    var Zebra = (function () {
        function Zebra() {
        }
        return Zebra;
    }());
    Animals.Zebra = Zebra;
    function animalsHaveMuscles() {
        return haveMuscles;
    }
    Animals.animalsHaveMuscles = animalsHaveMuscles;
})(Animals || (Animals = {}));
var Animals;
(function (Animals) {
    var Dog = (function () {
        function Dog() {
        }
        return Dog;
    }());
    Animals.Dog = Dog;
    function doAnimalsHaveMuscles() {
        return Animals.animalsHaveMuscles();
    }
    Animals.doAnimalsHaveMuscles = doAnimalsHaveMuscles;
})(Animals || (Animals = {}));
var Album = (function () {
    function Album() {
    }
    return Album;
}());
var Album;
(function (Album) {
    var AlbumLabel = (function () {
        function AlbumLabel() {
        }
        return AlbumLabel;
    }());
    Album.AlbumLabel = AlbumLabel;
})(Album || (Album = {}));
function buildLabel(name) {
    return buildLabel.prefix + name + buildLabel.suffix;
}
var buildLabel;
(function (buildLabel) {
    buildLabel.suffix = "!";
    buildLabel.prefix = "Hello, ";
})(buildLabel || (buildLabel = {}));
log(buildLabel("Pina Colada"));
var Color;
(function (Color) {
    Color[Color["red"] = 1] = "red";
    Color[Color["green"] = 2] = "green";
    Color[Color["blue"] = 4] = "blue";
})(Color || (Color = {}));
var Color;
(function (Color) {
    function mixRedGreen() {
        return Color.red + Color.green;
    }
    Color.mixRedGreen = mixRedGreen;
})(Color || (Color = {}));
log(Color.mixRedGreen());

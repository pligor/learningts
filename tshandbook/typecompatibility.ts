/**
 * Created by pligor on 4/16/15.
 */

function log(s:any):void {
    console.log(s)
}

interface Named {
    name: string;
}

var var1:Named = <Named>{name: "Geo"}
var var2 = {name: "Alice", location: "Seattle"}    //<--- anonymous instance

//this is ok because in Typescript we do NOT care if the names Named and anonymous are the same, only their structure
var1 = var2;
log(var1.name)
function greet(n:Named) {
    log("Hello " + n)
}
greet(var2) //this works because in Typescript we do NOT care if the names Named and anonymous are the same, only their structure


var func1 = (a:number) => 0;
var func2 = (b:number, s:string) => 0;
//we ask for two numbers in func2, we must get at least two
//func1 = func2;  //error func1 has too little parameters for func2 to fit in
func2 = func1;  //ok params are enough


var items = [1, 2, 3];
items.forEach((item, index, array) => log(index + ": " + item))
items.forEach(item => log(item))


var func3 = () => ({name: "Alice"})
var func4 = () => ({name: "Alice", location: "Dublin"})

func3 = func4 //func3 returns one value while func4 returns two values, so func4 returns enough values for us to have, so it is ok
//func4 = func3 //func4 returns two values while func3 returns only one value.

enum EventType { Mouse, Keyboard }

interface Event {
    timestamp: number;
}
interface MouseEvent extends Event {
    xPos: number;
    yPos: number;
}
interface KeyEvent extends Event {
    keyCode: number;
}

function listenEvent(eventType:EventType, handler:(n:Event) => void) {
    //...
}

//The caller is given a function that takes a more specialized type but it is invoked with the less specialized type
//so this works (unsound, but allowed):
listenEvent(EventType.Mouse, (e:MouseEvent) => console.log(e.x + ',' + e.y));
//so when the function is invoked inside the body of the listenEvent we should pass a less specialized structure

//Undesirable alternatives in presence of soundness.
//do casting to new type for each call of the "e" event object
listenEvent(EventType.Mouse, (e:Event) => log((<MouseEvent>e).x + "," + (<MouseEvent>e).y));
//OR do casting of the entire function
listenEvent(EventType.Mouse, <(e:Event) => void>((e:MouseEvent) => log(e.x + "," + e.y)))




//TODO ......... fill the rest
/**
 * Created by pligor on 4/16/15.
 */

function log(s:any):void {
    console.log(s)
}

var x = 3;


var y = [0, 1, null]

//var zoo = []

//mouseEvent is inferred because the onmousedown prototype of the function is known
window.onmousedown = function(mouseEvent) {
    log(mouseEvent.button)  //this works
    //log(mouseEvent.arbitraryFunc)  //this fails !!
}
//BUT we can set our own explicit type
window.onmousedown = function(mouseEvent: any) {
    log(mouseEvent.button)  //this works
    log(mouseEvent.arbitraryFunc)  //now this checks ok with the compiler (even though a runtime error is expected soon!
}



Array.prototype.unique = function () {
    return this.filter(function (elem, index, arr) {
        return arr.indexOf(elem) == index;
    });
};
Array.prototype.flat = function () {
    var flatarray = [];
    function func(arg) {
        if (Array.isArray(arg)) {
            for (var i = 0; i < arg.length; i++) {
                func(arg[i]);
            }
        }
        else {
            flatarray.push(arg);
        }
    }
    func(this);
    return flatarray;
};
var secretmap = {
    1: "α",
    2: "β",
    3: "α",
    4: "α",
    5: "α",
    6: "α",
    7: "α",
    8: "β",
    9: "α",
    10: "α",
    11: "α",
    12: "α",
    13: "α",
    14: "α",
    15: "α",
    16: "γ",
    17: "β",
    18: "α",
    19: "α",
    20: "β",
    21: "α",
    22: "γ",
    23: "β",
    24: "α",
};
var arr = [
    ["rb2", "rb1", "full1", "rb2", "bl3", "full2", "full3"],
    ["full1", "blt3", "full3"],
    ["bl3"],
    ["rlt2", "bl3", "rbt1", "bl2"],
    ["lt1", "full3", "rt3"],
];
console.log(arr.flat().unique().length);
console.log(arr.flat().unique());

//WHAT
var args = [4, 1, 66, 4];
console.log(args.slice(1, 100));
console.log(args.shift());

//console.log(typeof isNimble)

var canFly = function () {
    return true;
};

var bar = {}
bar.isDeadly = function () {
    return true;
};

console.log(typeof isNimble)

console.log(canFly() && bar.isDeadly() && isNimble())

function isNimble() {
    return true
}

function awesome() {
    var str = getSome() + "!"

    return str

    function getSome() {
        return "some"
    }
}

awesome()

function assert(check) {
    if (check === true) {
        //nop
    } else {
        throw "assertion failed"
    }
}

var ninja = function myNinja() {
    assert(ninja === myNinja)   //ninja is already available inside the function ??!!!! WTF
}

ninja()

var powerninja = {
    yell: function yell(n) {
        return n > 0 ? arguments.callee(n - 1) + "a" : "hiy"    //poly proxo
    }
}

console.log(
    powerninja.yell(5)
)

var samurai = {
    yell: powerninja.yell
}

//powerninja = null

try {
    console.log(
        samurai.yell(10)
    )
} catch (e) {
    console.log(e.message)
}

var obj = {}
var fn = function () {
}

obj.prop = "3"
fn.prop = 3
assert(obj.prop == fn.prop) //both objects

var db = {
    hello: 34,
    wold: 488
}

function log(str) {
    console.log(str)
}

function getArray(key) {
    if(this.cache === undefined) {
        this.cache = {}
    }

    //if (this.cache.hasOwnProperty(key)) {
    if (this.cache.hasOwnProperty(key)) {
        log("stored")
        return this.cache[key]
    } else {
        log("not stored")
        var retrieved = db[key]
        this.cache[key] = retrieved
        return retrieved
    }
}

console.log(
    getArray("hello")   //not stored
)

console.log(
    getArray("hello")   //stored
)

var katana = {
    isSharp: true,
    use: function() {
        //here "this" is the object that wraps the function
        this.isSharp = !this.isSharp
    }
}

log(katana.isSharp)
katana.use()
log(katana.isSharp)



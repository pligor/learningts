//WHAT

console.log(typeof null); // object //even in microsoft JScript

console.log(NaN === NaN)    //false ??!!


//scope inconsistencies
function foo(fn) {
    if (typeof fn === "function") {
        fn()
    }
}

var bar = {
    barbar: "Hello World",
    method: function () {
        console.log(this.barbar)
    }
}

bar.method()    //OK
foo(bar.method) //undefined ??? WTF ??? it's because of the this situation
foo(function () {   //this is ok again ??? :/
    bar.method()
})


console.log(!!(0))  //false ,should have been zero
console.log(!!(false))  //false as expected
console.log(!!("")) //false???? should have been empty string bitch
console.log(!!(null))   //false again ??? come on!!
console.log(!!(undefined))  //yep you guessed it false again
console.log(!!(NaN))    //by now anybody would think that anything is considered false in js :P

//no arithmetic
console.log(.2 + .4)    //0.6000000000000000000000001 what the fuck bitch !!!!???
console.log( (.2 + .4).toFixed(1) ) //fixed that indeed :P


var foof = function() {
    return
    {
        a: "b"
    }
}()

//console.log(foof.a) //EROORRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRr

var foofCorrect = function() {
    return {
        a: "b"
    }
}()

console.log(foofCorrect.a)  //no error .....
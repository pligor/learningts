function log(s) {
    console.log(s)
}

var url = require("url");

var app = {
    awesome: function(param) {
      return "indeed " + param
    }
}

log(
    url.resolve('/one/two/three', 'four')
)



var myObject = {
    price: 20.99,
    get_price : function() {
        return this.price;
    }
};

var customObject = Object.create(myObject);
customObject.price = 19.99;

console.log(customObject.price)
console.log(myObject == myObject)
console.log(customObject == myObject)

delete customObject.price

console.log( customObject.get_price() )